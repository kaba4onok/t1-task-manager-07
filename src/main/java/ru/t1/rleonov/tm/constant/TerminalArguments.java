package ru.t1.rleonov.tm.constant;

public final class TerminalArguments {

    public final static String CMD_ARG_VERSION = "-v";

    public final static String CMD_ARG_ABOUT = "-a";

    public final static String CMD_ARG_HELP = "-h";

    public final static String CMD_ARG_INFO = "-i";

}