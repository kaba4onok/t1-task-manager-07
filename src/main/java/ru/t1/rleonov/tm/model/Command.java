package ru.t1.rleonov.tm.model;

import ru.t1.rleonov.tm.constant.TerminalArguments;

import ru.t1.rleonov.tm.constant.TerminalCommands;

public class Command {

    public static  Command ABOUT = new Command(
            TerminalCommands.CMD_ABOUT, TerminalArguments.CMD_ARG_ABOUT,
            "Show application info."
    );

    public static  Command VERSION = new Command(
            TerminalCommands.CMD_VERSION, TerminalArguments.CMD_ARG_VERSION,
            "Show application version."
    );

    public static  Command EXIT = new Command(
            TerminalCommands.CMD_EXIT, null,
            "Exit application."
    );

    public static  Command HELP = new Command(
            TerminalCommands.CMD_HELP, TerminalArguments.CMD_ARG_HELP,
            "Show application commands."
    );

    private String name = "";

    private String argument;

    private String description = "";

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " --- " + description;
        return result;
    }

}
