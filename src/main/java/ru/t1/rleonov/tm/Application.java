package ru.t1.rleonov.tm;

import ru.t1.rleonov.tm.model.Command;

import ru.t1.rleonov.tm.util.FormatUtil;

import java.util.Scanner;

import static ru.t1.rleonov.tm.constant.TerminalCommands.*;

import static ru.t1.rleonov.tm.constant.TerminalArguments.*;

public final class Application {

    public static void main(final String[] args) {
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

    private static void processCommands() {
        System.out.println("***WELCOME TO TASK-MANAGER***");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_HELP:
                showHelp();
                break;
            case CMD_EXIT:
                exitApplication();
                break;
            case CMD_INFO:
                showInfo();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArguments(final String[] args) {
        final String arg = args[0];
        processArgument(arg);
    }

    private static void processArgument(final String arg) {
        switch (arg) {
            case CMD_ARG_ABOUT:
                showAbout();
                break;
            case CMD_ARG_VERSION:
                showVersion();
                break;
            case CMD_ARG_HELP:
                showHelp();
                break;
            case CMD_ARG_INFO:
                showInfo();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Task Manager");
        System.out.println("Developer: Roman Leonov");
        System.out.println("Gitlab: https://gitlab.com/kaba4onok/");
        System.out.println("E-mail: rleonov@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.HELP);
        System.out.println(Command.EXIT);
    }

    private  static  void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory (bytes): " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory (bytes): " + usageMemoryFormat);
    }

    private static void showErrorArgument() {
        System.err.println("Error! This argument is not supported.");
    }

    private static void showErrorCommand() {
        System.err.println("Error! This command is not supported.");
    }

    private static void exitApplication() {
        System.exit(0);
    }

}
